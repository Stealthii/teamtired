Crank: The Game
=================

Description
-------------

Crank: The Game is a top-down platform-style game which takes theme influence
from the movie Crank.  The goal is literally to stay alive - keep your heart
rate up to try and survive!  Watch out, if it gets too high, you will die, or
even too low, you poor fellow.

Enough.

This game uses the Python language and the PyGame library.  The game engine was
repurposed from a free software game titled "Ulmo's Adventure" (available at
http://www.pygame.org/project-Ulmo's+Adventure-2042-.html)

This project took place over 48 hours at the Global Game Jam 2013 on location
at Farset Labs, Belfast, United Kingdom. (http://farsetlabs.org.uk)

Contributors
--------------

There were 3 contributors to this project:

- **Andrew Sheppard** (andrew.sheppard93@hotmail.co.uk) - Graphics Designer
- **Claire Wilgar** (cwilgar@gmail.com) - Lead Development Engineer
- **Daniel Porter** (dpreid@gmail.com) - Developer / Project Lead

How to Install & Run
----------------------

As a python application, this can be run directly by running 'play.py' using
any Python 2.x interpreter (2.7 tested and recommended), like the following
command:

```bash

$ python play.py

```

To install, run 'setup.py' in the usual fashion, using easy_installer or pip.

Notable Bugs
--------------

We were unable to finish the flicker animation that occurs when hit
(invulnerability for 3 seconds), but left the feature in as it is crucial to
pass certain areas of the game.  Players should be aware of this when colliding
with enemies.

Unimplemented Ideas
---------------------

Due to time constraints we were unable to implement the following features:

- Flicker when hit (invulnerability)
- MIDI music of New Noise by Refused (Crank Soundtrack)
- Player speed being impacted by heart rate
- Movie influenced level design
- Weaponry
- Artificial Intelligence (Swarming, other NPCs)

Copyright
-----------
(C) 2013 Farset Labs LLU.

Licence
---------
See license.txt.
