GLOBAL GAME JAM 2013
====================

Claire''s notes
--------------

- based on heart rate
- keep heart rate either above or below a certain rate depending on the level
- die when - heart passes min/max rate
- relationship between danger levels and heart rate (and controls?)
- horror theme, scary music and darkness etc
- side scroller/platform game
- game ends when you die (or complete all the levels?)



