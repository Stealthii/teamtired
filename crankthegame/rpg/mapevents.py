#!/usr/bin/env python

from view import DOWN

DUMMY_EVENT = 0
TILE_EVENT = 1
BOUNDARY_EVENT = 2

BOUNDARY_TRANSITION = 1
SCENE_TRANSITION = 2
LIFE_LOST_TRANSITION = 3
GAME_OVER_TRANSITION = 4
END_GAME_TRANSITION = 5

EMPTY_LIST = []


class MapEvent:

    """
    There are two parts to each map event:
    1. The event itself, eg. a BoundaryEvent indicates that the player has breached a
    map boundary.
    2. A transition that describes what happens next, eg. a SceneTransition indicates
    that we need to replace the current map with another map.

    For example, a BoundaryEvent might result in a BoundaryTransition, when the
    player walks off the edge of one map and onto another, OR a SceneTransition,
    when the player walks out of a cave for example.
    """

    def __init__(self, type, transition=None):
        self.type = type
        self.transition = transition


class DummyEvent(MapEvent):

    """Defines an event that doesn't do anything."""

    def __init__(self, boundary=None):
        MapEvent.__init__(self, DUMMY_EVENT)
        self.boundary = boundary


class TileEvent(MapEvent):

    """Defines an event that occurs when the player steps on a tile that has an event."""
    def __init__(self, transition, x, y, level):
        MapEvent.__init__(self, TILE_EVENT, transition)
        self.x, self.y = x, y
        self.level = level


class BoundaryEvent(MapEvent):

    """Defines an event that occurs when the player walks off the edge of the map."""
    def __init__(self, transition, boundary, min, max=None):
        MapEvent.__init__(self, BOUNDARY_EVENT, transition)
        self.boundary = boundary
        if max:
            self.range = range(min, max + 1)
        else:
            self.range = [min]


class Transition:

    """Transition base class."""
    def __init__(self, type, mapName=None):
        self.type = type
        self.mapName = mapName


class SceneTransition(Transition):

    """
    Defines a transition that occurs when we switch from one scene to another, eg.
    when the player walks into a cave.
    """

    def __init__(self, mapName, x, y, level, direction, boundary=None):
        Transition.__init__(self, SCENE_TRANSITION, mapName)
        self.tilePosition = (x, y)
        self.level = level
        self.direction = direction
        self.boundary = boundary


class LifeLostTransition(Transition):

    """
    Defines a transition that occurs when the player loses a life and the scene is
    reset.  Note that this is very similar to a scene transition.
    """
    def __init__(self, mapName, x, y, level):
        Transition.__init__(self, LIFE_LOST_TRANSITION, mapName)
        self.tilePosition = (x, y)
        self.level = level
        self.direction = DOWN
        self.boundary = None


class BoundaryTransition(Transition):

    """
    Defines a transition that occurs when the player walks off the edge of one map
    and onto another.
    """
    def __init__(self, mapName, boundary, modifier=0):
        Transition.__init__(self, BOUNDARY_TRANSITION, mapName)
        self.boundary = boundary
        self.modifier = modifier


class GameOverTransition(Transition):

    def __init__(self):
        Transition.__init__(self, GAME_OVER_TRANSITION)


class EndGameTransition(Transition):

    def __init__(self):
        Transition.__init__(self, END_GAME_TRANSITION)
