#!/usr/bin/env python

from distutils.core import setup

setup(name="crankthegame",
      version="0.1",
      py_modules=["play"],
      packages=["rpg"],
      author="TeamTired",
      author_email="teamtired@hmamail.com",
      url="http://farsetlabs.org.uk/"
      )
